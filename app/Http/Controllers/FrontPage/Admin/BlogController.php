<?php

namespace App\Http\Controllers\FrontPage\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index(){
        $blog = Blog::where('cat','admin')->get();

        return view('frontpage.main.blog.index',compact('blog'));
    }

    public function show($slug){
        $blog = Blog::where('slug',$slug)->first();

        return view('frontpage.main.blog.detail',compact('blog'));
    }
}
