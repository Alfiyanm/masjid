<?php

namespace App\Http\Controllers\FrontPage\Takmir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Takmir;
use App\User;
use App\Models\Organization;
use App\Models\JobdescOrg;
use App\Models\Mosque;

class WebPageController extends Controller
{
    public function index($uuid)
    {
        $user = User::where('uuid',$uuid)->first();
        $takmir = Takmir::where('user_id',$user->id)->first();
        $org = Organization::where('takmir_id',$takmir->id)->first();
        $job = JobdescOrg::where('takmir_id',$takmir->id)->first();
        $mosque = Mosque::where('takmir_id',$takmir->id)->first();
        $data = [
            'user' =>$user,
            'takmir'=> $takmir,
            'org' => $org,
            'job' => $job,
            'mosque' => $mosque
        ];

        return view('frontpage.takmir.index',compact('data'));
    }
}
