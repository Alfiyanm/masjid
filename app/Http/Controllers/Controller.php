<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Yajra\DataTables\DataTables;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function displayTakmir($user)
    {
        return DataTables::of($user)
        ->addColumn('action', function ($user) {
            $url = route('takmir.show',$user->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('takmir.edit',$user->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deleteUser(".$user->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->make(true);
    }

    public function displayAdmin($user)
    {
        return DataTables::of($user)
        ->addColumn('action', function ($user) {
            $url = route('admin.show',$user->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('admin.edit',$user->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deleteUser(".$user->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->make(true);
    }

    public function displayJamaah($jamaah)
    {
        return DataTables::of($jamaah)
        ->addColumn('action', function ($jamaah) {
            $url = route('jamaah.show',$jamaah->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('jamaah.edit',$jamaah->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deleteUser(".$jamaah->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->make(true);
    }

    public function displayBlog($blog)
    {
        return DataTables::of($blog)
        ->addColumn('action', function ($blog) {
            $url = route('blog.show',$blog->uuid);
            return "<a style='margin-top:0.5em; width:10em;' href='".$url."' class='btn btn-info btn-outline btn-xs' target='_blank'><i class='fa fa-eye'> Detail</i></a>&nbsp;".
            "<a style='margin-top:0.5em; width:10em;' href='".route('blog.edit',$blog->uuid)."' class='btn btn-success btn-outline btn-xs' target='_blank'><i class='fa fa-pencil'> Edit</i></a> ".
            "<a style='margin-top:0.5em; width:10em;' onclick='deletePost(".$blog->id.")' class='btn btn-danger btn-outline btn-xs text-white'><i class='fa fa-trash'> Delete</i></a>";
        })->editColumn('created_at',function($blog){
            $date = date('d - m - Y',strtotime($blog->created_at));
            return $date;
        })
        ->make(true);
    }

}
