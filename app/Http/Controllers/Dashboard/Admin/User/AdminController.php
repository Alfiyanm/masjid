<?php

namespace App\Http\Controllers\Dashboard\Admin\User;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Models\UserRole;
use App\Models\Admin;

class AdminController extends Controller
{
    public function index()
    {
        return view('dashboard.admin.user.admin.all_user');        
    }

    
    public function create()
    {
        return view('dashboard.admin.user.admin.add_user');
    }
    
    public function store(Request $request)
    {
        $user = new User;

        $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required|unique:users',
            'password' => 'required|min:8'
        ]);
        
        $user->uuid = Uuid::uuid4();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        Admin::create(['user_id'=>$user->id]);
        UserRole::create(['user_id' => $user->id,'role_id' => 3]);
        UserRole::create(['user_id' => $user->id,'role_id' => 2]);

        return redirect()->route('admin.index')->with('success','Berhasil Menambahkan'); 
    }

    
    public function show($id)
    {
        $user = User::where('uuid',$id)->first();
        $admin = $user->admin()->first();

        return view('dashboard.admin.user.admin.detail_user',compact('user','admin'));
    }
    
    public function edit($id)
    {
        $user = User::where('uuid',$id)->first();
        $admin = $user->admin()->first();

        return view('dashboard.admin.user.admin.edit_user',compact('user','admin'));
    }
    
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'email' => 'required',
            'password' => 'required|min:8'
        ]);
        
        $user = User::where('uuid',$id)->first();
        $admin = $user->admin()->first();

        //============ User =========
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->password != $user->password) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        //=========== admin =============
        $admin->province = $request->province;
        $admin->address = $request->address;
        $admin->hp = $request->hp;
        $admin->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $admin->birth_place = $request->birth_place;
        $admin->gender = $request->gender;

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $user->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/user',$photoName);
            $admin->photo = $photoName;
        }
        $admin->save();

        return redirect()->back()->with('success','Edit Success');
    }
    
    public function destroy($id)
    {
        $admin = Admin::find($id);
        $user = User::where('id',$admin->user_id)->first();
        // dd($user);

        $user->delete();
    }

    public function adminApi(){
        $user = DB::table('users')->rightJoin('admins','admins.user_id','=','users.id');

        return $this->displayAdmin($user);
    }
}
