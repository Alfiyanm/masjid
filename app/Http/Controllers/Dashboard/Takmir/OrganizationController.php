<?php

namespace App\Http\Controllers\Dashboard\Takmir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\JobdescOrg;

class OrganizationController extends Controller
{
    public function index($uuid)
    {
        $org = Organization::where('uuid',$uuid)->first();

        return view('dashboard.takmir.organization.organization',compact('org'));
    }
    public function editStructure($uuid)
    {
        $org = Organization::where('uuid',$uuid)->first();
        return view('dashboard.takmir.organization.structure',compact('org'));
    }

    public function updateStructure(Request $request, $uuid)
    {
        Organization::where('uuid', $uuid)->update([
            'pelindung' => $request['pelindung'],
            'penasehat' => $request['penasehat'],
            'ketua' => $request['ketua'],
            'sekretaris' => $request['sekretaris'],
            'bendahara' => $request['bendahara'],
            'humas' => $request['humas'],
            'keagamaan' => $request['keagamaan'],
            'kepemudaan' => $request['kepemudaan'],
            'pelaksana_umum' => $request['pelaksana_umum'],
            'tarbiyah' => $request['tarbiyah'],
            'pengajian' => $request['pengajian'],
            'imam' => $request['imam'],
            'muadzin' => $request['muadzin'],
            'remaja' => $request['remaja'],
            'keamanan' => $request['keamanan'],
            'marbot' => $request['marbot'],
        ]);

        return redirect()->route('org',$uuid)->with('success','Berhasil Merubah');

    }

    public function editJobdesc($uuid)
    {
        $job = JobdescOrg::where('uuid',$uuid)->first();
        return view('dashboard.takmir.organization.jobdesc',compact('job'));
    }

    public function updateJobdesc(Request $request, $uuid)
    {
        JobdescOrg::where('uuid', $uuid)->update([
            'pelindung' => $request['pelindung'],
            'penasehat' => $request['penasehat'],
            'ketua' => $request['ketua'],
            'sekretaris' => $request['sekretaris'],
            'bendahara' => $request['bendahara'],
            'humas' => $request['humas'],
            'keagamaan' => $request['keagamaan'],
            'kepemudaan' => $request['kepemudaan'],
            'pelaksana_umum' => $request['pelaksana_umum'],
            'tarbiyah' => $request['tarbiyah'],
            'pengajian' => $request['pengajian'],
            'imam' => $request['imam'],
            'muadzin' => $request['muadzin'],
            'remaja' => $request['remaja'],
            'keamanan' => $request['keamanan'],
            'marbot' => $request['marbot'],
        ]);

        return redirect()->back()->with('success','Berhasil Merubah');

    }
}
