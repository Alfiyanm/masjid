<?php

namespace App\Http\Controllers\Dashboard\Takmir;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Jamaah;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Takmir;

class JamaahController extends Controller
{
    public function index()
    {
        return view('dashboard.takmir.jamaah.all_user');        
    }

    
    public function create()
    {
        return view('dashboard.takmir.jamaah.add_user');
    }
    
    public function store(Request $request)
    {
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        $jamaah = new Jamaah;

        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg',
        ]);
        
        $jamaah->uuid = Uuid::uuid4();
        $jamaah->takmir_id = $takmir->id;
        $jamaah->name = $request->name;
        $jamaah->address = $request->address;
        $jamaah->province = $request->province;
        $jamaah->hp = $request->hp;
        $jamaah->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $jamaah->birth_place = $request->birth_place;
        $jamaah->gender = $request->gender;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $jamaah->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/jamaah',$photoName);
            $jamaah->photo = $photoName;
        }

        $jamaah->save();

        return redirect()->route('jamaah.index')->with('success','Berhasil Menambahkan'); 
    }

    
    public function show($id)
    {
        $jamaah = Jamaah::where('uuid',$id)->first();

        // dd($mosque);

        return view('dashboard.takmir.jamaah.detail_user',compact('jamaah'));
    }
    
    public function edit($id)
    {
        $jamaah = Jamaah::where('uuid',$id)->first();

        // dd($mosque);

        return view('dashboard.takmir.jamaah.edit_user',compact('jamaah'));
    }
    
    public function update(Request $request, $id)
    {
        
        $jamaah = Jamaah::where('uuid',$id)->first();

        //============ User =========
        $jamaah->name = $request->name;
        $jamaah->province = $request->province;
        $jamaah->address = $request->address;
        $jamaah->hp = $request->hp;
        $jamaah->birth_date = date('Y-m-d', strtotime($request->birth_date));
        $jamaah->birth_place = $request->birth_place;
        $jamaah->gender = $request->gender;

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = $jamaah->uuid.$photo->getClientOriginalName();
            $photo->storeAs('public/jamaah',$photoName);
            $jamaah->photo = $photoName;
        }
        $jamaah->save();


        return redirect()->back()->with('success','Berhasil Menyimpan Perubahan');
    }
    
    public function destroy($id)
    {
        $jamaah = Jamaah::where('id',$id)->first();
        $jamaah->delete();

        // return redirect()->route('user.index')->with('danger','Berhasil Menghapus');
    }

    public function jamaahApi(){
        $takmir = DB::table('takmirs')->where('user_id',Auth::user()->id)->first();
        $jamaah = DB::table('jamaahs')->where('takmir_id',$takmir->id)->get();

        return $this->displayJamaah($jamaah);
    }
}
