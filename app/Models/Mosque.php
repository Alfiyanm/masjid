<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mosque extends Model
{
    protected $guarded = ['id'];
    protected $table = 'mosques';

    public function takmir(){
        return $this->belongsTo('App\Models\Takmir');
    }
}
