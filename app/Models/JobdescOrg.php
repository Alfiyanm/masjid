<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobdescOrg extends Model
{
    protected $guarded = ['id'];
    protected $table = 'jobdesc_orgs';

    public function masjid(){
        return $this->belongsTo('App\Models\Takmir');
    }
}
