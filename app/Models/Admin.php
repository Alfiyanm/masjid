<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $guarded = ['id'];
    protected $table = 'admins';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
