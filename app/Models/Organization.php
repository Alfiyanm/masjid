<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $guarded = ['id'];
    protected $table = 'organizations';

    public function masjid(){
        return $this->belongsTo('App\Models\Takmir');
    }
}
