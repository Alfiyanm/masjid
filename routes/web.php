<?php
use App\Htttp\Middleware\CheckRole;


Route::get('/', function () {
    return view('frontpage.main.index');
})->name('home.index');
Route::get('/news','FrontPage\Admin\BlogController@index')->name('news');
Route::get('/news/{slug}','FrontPage\Admin\BlogController@show')->name('news.show');

Route::get('web/{uuid}','FrontPage\Takmir\WebPageController@index')->name('web');

Auth::routes();
Auth::routes(['verify' => true]);

Route::group(['middleware'=>['auth','verified']], function () {
    Route::get('test',function(){
        return view('dashboard.takmir.organization.organization');
    });
    Route::get('/home', 'HomeController@index')->name('home');

    // Blog
    Route::resource('blog','Dashboard\BlogController');
    Route::get('api.blog','Dashboard\BlogController@blogApi')->name('api.blog');

    // Jamah
    Route::resource('jamaah', 'Dashboard\Takmir\JamaahController');
    Route::get('api.jamaah', 'Dashboard\Takmir\JamaahController@jamaahApi')->name('api.jamaah');

    // Organization Start
    Route::get('org/{uuid}','Dashboard\Takmir\OrganizationController@index')->name('org');
    
    Route::get('org/{uuid}/edit','Dashboard\Takmir\OrganizationController@editStructure')->name('org.edit');
    Route::PATCH('org/{uuid}/update','Dashboard\Takmir\OrganizationController@updateStructure')->name('org.update');

    Route::get('org/job/{uuid}/edit','Dashboard\Takmir\OrganizationController@editJobdesc')->name('org.editjob');
    Route::PATCH('org/job/{uuid}/update','Dashboard\Takmir\OrganizationController@updateJobdesc')->name('org.updatejob');
    // Organization End

    // Profile
    Route::get('profile','Dashboard\Takmir\ProfileController@index')->name('profile');
    Route::get('profile/edit/{uuid}','Dashboard\Takmir\ProfileController@edit')->name('profile.edit');
    Route::PATCH('profile/update/{id}','Dashboard\Takmir\ProfileController@update')->name('profile.update');
    Route::get('profile/passEdit/{uuid}','Dashboard\Takmir\ProfileController@passEdit')->name('profile.passEdit');
    Route::PATCH('profile/passUpdate/{uuid}','Dashboard\Takmir\ProfileController@passUpdate')->name('profile.passUpdate');


    Route::resource('takmir', 'Dashboard\Admin\User\TakmirController');

    Route::group(['middleware' => ['role:admin']], function () {

        Route::get('api.takmir', 'Dashboard\Admin\User\TakmirController@takmirApi')->name('api.takmir');

        Route::resource('admin', 'Dashboard\Admin\User\AdminController');
        Route::get('api.admin', 'Dashboard\Admin\User\AdminController@adminApi')->name('api.admin');

    });
});
