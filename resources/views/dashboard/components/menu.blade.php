<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      {{-- <li class="nav-item nav-profile">
        <div class="nav-link">
          <div class="user-wrapper">
            <div class="profile-image">
              <img src="{{asset('img/face1.jpg')}}" alt="profile image">
            </div>
            <div class="text-wrapper">
              <p class="profile-name">{{Auth::user()->name}}</p>
              <div>
                <small class="designation text-muted"> </small>
                <span class="status-indicator online"></span>
              </div>
            </div>
          </div>
          <button class="btn btn-success btn-block">New Project
            <i class="mdi mdi-plus"></i>
          </button>
        </div>
      </li> --}}
      <li class="nav-item">
        <a class="nav-link" href="{{route('home')}}">
          <i class="menu-icon mdi mdi-television"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#artikel" aria-expanded="false" aria-controls="artikel">
          <i class="menu-icon mdi mdi-lead-pencil"></i>
          <span class="menu-title">Artikel</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="artikel">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('blog.index')}}">Semua Artikel</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('blog.create')}}">Tambah Artikel</a>
            </li>
          </ul>
        </div>
      </li>
      @if (!Auth::user()->hasRole('master') || !Auth::user()->hasRole('admin'))
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#jamaah" aria-expanded="false" aria-controls="jamaah">
          <i class="menu-icon mdi mdi-account"></i>
          <span class="menu-title">Jamaah</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="jamaah">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('jamaah.index')}}">Semua Jamaah</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('jamaah.create')}}">Tambah Jamaah</a>
            </li>
          </ul>
        </div>
      </li>
      @endif
      @if (Auth::user()->hasRole('admin'))
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <i class="menu-icon mdi mdi-account"></i>
          <span class="menu-title">Users</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('takmir.index')}}">Semua Takmir</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('takmir.create')}}">Tambah Takmir</a>
            </li>
          </ul>
        @if (Auth::user()->hasRole('master'))
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="{{route('admin.index')}}">Semua Admin</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{route('admin.create')}}">Tambah Admin</a>
            </li>
          </ul>
        </div>
        @endif
      </li>
      @endif
    </ul>
  </nav>
  <!-- partial -->