@extends('dashboard.master')
@section('title')
Tambah Admin
@endsection
@section('content')
<div class="row">
    <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Tambah Admin</h4>
              <form class="forms-sample" method="POST" action="{{route('admin.store')}}" enctype="multipart/form-data">
                  {{csrf_field()}} {{ method_field('POST')}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input required type="text" class="form-control" name="name" value="{{ old('name') }}" id="name" placeholder="Masukan Nama">
                    @error('name')
                      <span class="mt-1 alert-danger" role="alert">
                        <small>{{ $message }}</small>
                      </span>
                    @enderror
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input value="{{ old('email') }}" required type="email" class="form-control" name="email" id="email" placeholder="Masukan Email">
                  @error('email')
                      <span class="mt-1 alert-danger" role="alert">
                        <small>{{ $message }}</small>
                      </span>
                  @enderror
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input required type="password" class="form-control" id="password" name="password" placeholder="Password">
                  @error('password')
                      <span class="mt-1 alert-danger" role="alert">
                        <small>{{ $message }}</small>
                      </span>
                  @enderror
                </div>
                <button type="submit" class="btn btn-success mr-2">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection

