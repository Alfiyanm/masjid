@extends('dashboard.master')
@section('title')
Semua Admin
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                @include('layouts.patrials.alerts')
                <div class="row">
                    <div class="col-md-6"><h4 class="card-title"> Semua Admin </h4></div>
                    <div class="col-md-6"><a class="btn btn-success float-right" href="{{route('admin.create')}}">Tambah Admin</a></div>
                </div>
                <div class="center-pos" id="loadingDiv">
                        <img class="loading" src="{{asset('img/loading.gif')}}">
                </div>
                <div class="table-responsive">
                    <table id="table-admin" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Telepon</th>
                                <th>Gender</th>
                                <th>Provinsi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
    
    var ajaxApi = '{{route("api.admin")}}'
    var table = $('#table-admin').DataTable({
        order: [[1,'desc']],
        processing: true,
        serverSide: true,
        ajax : ajaxApi,
        columns : [
            {data: 'name', name: 'name'},
            {data: 'hp', name: 'hp'},
            {data: 'gender', name: 'gender'},
            {data: 'province', name: 'province'},
            {data: 'action', name: 'action',ordertable: false, searchable:false},
        ]

    })
    

    // Delete Data
    function deleteUser(id){
        // alert('bismillah');
        
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        Swal.fire({
          title: 'Kamu yakin ?',
          text: "User yang di hapus tidak akan kembali!",
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Ya, Hapus ini!'
        }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "{{ url('admin') }}" + '/' + id,
            type: "DELETE",
            data: {'_method' : 'DELETE', '_token' : csrf_token},
            success: function(data){
              table.ajax.reload();
              swal({
                title:'Berhasil',
                text: 'User Sudah di hapus',
                type: 'success',
                timer: '1500'
              })
            },
            error: function(){
              swal({
                title: 'Oops',
                text: 'Something went wrong',
                type: 'error',
                timer: '1500'
              })
            }
          })
        }
      })
    }

    
    </script>
@endsection