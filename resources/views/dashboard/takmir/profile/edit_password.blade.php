@extends('dashboard.master')
@section('title')
    Edit Jamaah
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Perbarui Password</h2>
                <form class="form-sample" method="POST" action="{{route('profile.passUpdate',$user->uuid)}}"
                    enctype="multipart/form-data">
                    {{csrf_field()}} {{ method_field('PATCH')}}

                    {{-- Personal Info --}}
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="card-description">
                                Perbarui Password
                            </h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input value="" name="pass" id="pass" type="password"
                                                class="form-control">
                                                <small>Minimal 8 karakter</small>
                                            @error('pass')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Ulangi Password</label>
                                        <div class="col-sm-9">
                                            <input value="" name="cpass" id="cpass" type="password"
                                                class="form-control">
                                            @error('cpass')
                                            <span class="mt-1 alert-danger" role="alert">
                                                <small>{{ $message }}</small>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block" value="Edit">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('js/jquery-ui.js')}}"></script>
@endsection