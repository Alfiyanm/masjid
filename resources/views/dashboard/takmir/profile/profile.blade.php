@extends('dashboard.master')
@section('title')
Struktur Organisasi
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body card-width">
                <a class="btn btn-info" href="{{route('org',$data['org']->uuid)}}">Struktur Organisasi</a>
                <a class="btn btn-success" href="{{route('org.edit',$data['org']->uuid)}}">Edit Struktur Organisasi</a>
                <a class="btn btn-danger" href="{{route('org.editjob',$data['job']->uuid)}}">Edit Jobdesc Organisasi</a>
                <br>
                <br>
                <a class="btn btn-primary" href="">Lihat Profile</a>
                <a class="btn btn-warning" href="{{route('profile.edit',$data['user']->uuid)}}">Edit Profile</a>
                <a class="btn btn-info" href="{{route('profile.passEdit',$data['user']->uuid)}}">Edit Password</a>
                <br>
                <br>
                <a target="_blank" class="btn btn-info btn-block" href="{{route('web',$data['user']->uuid)}}">LIhat WEB Profile</a>
            </div>
        </div>
    </div>
</div>
@endsection