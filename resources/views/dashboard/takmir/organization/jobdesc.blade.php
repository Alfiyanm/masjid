@extends('dashboard.master')
@section('title')
Jobdesc
@endsection
@section('content')
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h3 class="">Jobdesc Organisasi Masjid</h3>
                <form class="form-sample" method="POST" action="{{route('org.updatejob',$job->uuid)}}">
                        {{csrf_field()}} {{ method_field('PATCH')}}
                        <div class="row justify-content-center ">
                            <div class="col-md-4 form-group">
                                <label>Pelindung</label>
                                <textarea class="form-control wysiwyg" name="pelindung" id="pelindung" cols="30" rows="5">{{$job->pelindung}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Penasehat</label>
                                <textarea class="form-control wysiwyg" name="penasehat" id="penasehat" cols="30" rows="5">{{$job->penasehat}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Ketua</label>
                                <textarea class="form-control wysiwyg" name="ketua" id="ketua" cols="30" rows="5">{{$job->ketua}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Sekretaris</label>
                                <textarea class="form-control wysiwyg" name="sekretaris" id="sekretaris" cols="30" rows="5">{{$job->sekretaris}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Bendahara</label>
                                <textarea class="form-control wysiwyg" name="bendahara" id="bendahara" cols="30" rows="5">{{$job->bendahara}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Humas</label>
                                <textarea class="form-control wysiwyg" name="humas" id="humas" cols="30" rows="5">{{$job->humas}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Keagamaan</label>
                                <textarea class="form-control wysiwyg" name="keagamaan" id="keagamaan" cols="30" rows="5">{{$job->keagamaan}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Kepemudaan</label>
                                <textarea class="form-control wysiwyg" name="kepemudaan" id="kepemudaan" cols="30" rows="5">{{$job->kepemudaan}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Pelaksana Umum</label>
                                <textarea class="form-control wysiwyg" name="pelaksana_umum" id="pelaksana_umum" cols="30" rows="5">{{$job->pelaksana_umum}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Tarbiyah</label>
                                <textarea class="form-control wysiwyg" name="tarbiyah" id="tarbiyah" cols="30" rows="5">{{$job->tarbiyah}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Pengajian</label>
                                <textarea class="form-control wysiwyg" name="pengajian" id="pengajian" cols="30" rows="5">{{$job->pengajian}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Imam</label>
                                <textarea class="form-control wysiwyg" name="imam" id="imam" cols="30" rows="5">{{$job->imam}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Muadzin</label>
                                <textarea class="form-control wysiwyg" name="muadzin" id="muadzin" cols="30" rows="5">{{$job->muadzin}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Remaja</label>
                                <textarea class="form-control wysiwyg" name="remaja" id="remaja" cols="30" rows="5">{{$job->remaja}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Keamanan</label>
                                <textarea class="form-control wysiwyg" name="keamanan" id="keamanan" cols="30" rows="5">{{$job->keamanan}}</textarea>
                            </div>
                            <div class="col-md-4 form-group">
                                <label>Marbot</label>
                                <textarea class="form-control wysiwyg" name="marbot" id="marbot" cols="30" rows="5">{{$job->marbot}}</textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection