@extends('dashboard.master')
@section('title')
Struktur Organisasi
@endsection
@section('css')
<style>
    .card-title {
        font-size: 10px !important;
    }

    .card-text {
        font-size: 10px !important;
    }

    .card-width {
        width: 150%;
    }
</style>
@endsection
@section('content')
<?php
    function newline($contain){
        $e = explode(',',$contain);
        $i = implode('<br>',$e);
        return $i;

    }
?>
<div class="row">
    @include('layouts.patrials.alerts')
    <div class="col-12 grid-margin p-0">
    <a class="btn my-3 btn-info" href="{{route('org.edit',$org->uuid)}}">Edit Struktur Organisasi</a>
        <div class="card p-0 scroll">
            <div class="card-body p-0 card-width">
                <div class="tree row justify-content-center ">
                    <ul>
                        <li class="row justify-content-center text-sm">
                            <div class="card border px-0 border-dark w-50">
                                <div class="card-body">
                                    <h4 class="card-title">Pelindung</h4>
                                    <hr>
                                    <p class="card-text">{!! newline($org->pelindung)  !!}</p>
                                </div>
                            </div>
                            <ul>
                                <li class="row justify-content-center">
                                    <div class="card border px-0 border-dark w-50">
                                        <div class="card-body">
                                            <h4 class="card-title">Penasehat</h4>
                                            <hr>
                                            <p class="card-text">{!! $org->penasehat !!}</p>
                                        </div>
                                    </div>
                                    <ul>
                                        <li class="row justify-content-center">
                                            <div class="card border px-0 border-dark w-50">
                                                <div class="card-body">
                                                    <h4 class="card-title">Ketua</h4>
                                                    <hr>
                                                    <p class="card-text">{!! $org->ketua !!}</p>
                                                </div>
                                            </div>
                                            <ul>
                                                <li>
                                                    <div class="card border px-0 border-dark">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Bendahara</h4>
                                                            <hr>
                                                            <p class="card-text">{!! $org->bendahara !!}</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="card border px-0 border-dark">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Sekretaris</h4>
                                                            <hr>
                                                            <p class="card-text">{!! $org->sekretaris !!}</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="card border px-0 border-dark">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Humas</h4>
                                                            <hr>
                                                            <p class="card-text">{!! $org->humas !!}</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="card border px-0 border-dark">
                                                        <div class="card-body">
                                                            <h4 class="card-title font-org">Keagamaan</h4>
                                                            <hr>
                                                            <p class="card-text">{!! $org->keagamaan !!}</p>
                                                        </div>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <div class="card border px-0 border-dark">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Tarbiyah</h4>
                                                                    <hr>
                                                                    <p class="card-text">{!! $org->tarbiyah !!}</p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="card border px-0 border-dark">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Pengajian</h4>
                                                                    <hr>
                                                                    <p class="card-text">{!! $org->pengajian !!}</p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="card border px-0 border-dark">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Imam</h4>
                                                                    <hr>
                                                                    <p class="card-text">{!! $org->imam !!}</p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="card border px-0 border-dark">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Muadzin</h4>
                                                                    <hr>
                                                                    <p class="card-text">{!! $org->muadzin !!}</p>
                                                                </div>
                                                            </div>
                                                        </li>

                                                    </ul>
                                                </li>
                                                <li>
                                                    <div class="card border px-0 border-dark">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Kepemudaan</h4>
                                                            <hr>
                                                            <p class="card-text">{!! $org->kepemudaan !!}</p>
                                                        </div>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <div class="card border px-0 border-dark">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Remaja</h4>
                                                                    <hr>
                                                                    <p class="card-text">{!! $org->remaja !!}</p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <div class="card border px-0 border-dark">
                                                        <div class="card-body">
                                                            <h4 class="card-title">Pelaksana Umum</h4>
                                                            <hr>
                                                            <p class="card-text">{!! $org->pelaksana_umum !!}</p>
                                                        </div>
                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <div class="card border px-0 border-dark">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Keamanan</h4>
                                                                    <hr>
                                                                    <p class="card-text">{!! $org->keamanan !!}</p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="card border px-0 border-dark">
                                                                <div class="card-body">
                                                                    <h4 class="card-title">Marbot</h4>
                                                                    <hr>
                                                                    <p class="card-text">{!! $org->marbot !!}</p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection