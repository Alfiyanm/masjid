@extends('frontpage.main.master')
@section('content')
@include('frontpage.main.header')
@include('frontpage.main.home')
@include('frontpage.main.intro')
{{-- @include('frontpage.main.work') --}}
{{-- @include('frontpage.main.testimonial') --}}
@include('frontpage.main.service')    
@include('frontpage.main.about')
{{-- @include('frontpage.main.counter') --}}
@include('frontpage.main.contact')
@include('frontpage.main.footer')
@endsection