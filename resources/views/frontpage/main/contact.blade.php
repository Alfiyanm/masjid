<section id="fh5co-contact" data-section="contact">
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-12 section-heading text-center">
                <h2 class="to-animate">Get In Touch</h2>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 subtext to-animate">
                        <h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                            there live the blind texts.</h3>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row row-bottom-padded-md justify-content-center">
            <div class="col-md-12 to-animate">
                <h3>Contact Info</h3>
                <ul class="fh5co-contact-info">
                    <li class="fh5co-contact-address ">
                        <i class="icon-home"></i>
                        Glagah Lor, Tamanan, Banguntapan, Bantul Regency, Special Region of Yogyakarta 55191<br>Indonesia
                    </li>
                    <li><i class="icon-phone"></i> +62 852-2880-2828</li>
                    <li><i class="icon-envelope"></i>info@pondokit.com</li>
                </ul>
            </div>

            {{-- <div class="col-md-6 to-animate">
                <h3>Contact Form</h3>
                <div class="form-group ">
                    <label for="name" class="sr-only">Name</label>
                    <input id="name" class="form-control" placeholder="Name" type="text">
                </div>
                <div class="form-group ">
                    <label for="email" class="sr-only">Email</label>
                    <input id="email" class="form-control" placeholder="Email" type="email">
                </div>
                <div class="form-group ">
                    <label for="phone" class="sr-only">Phone</label>
                    <input id="phone" class="form-control" placeholder="Phone" type="text">
                </div>
                <div class="form-group ">
                    <label for="message" class="sr-only">Message</label>
                    <textarea name="" id="message" cols="30" rows="5" class="form-control"
                        placeholder="Message"></textarea>
                </div>
                <div class="form-group ">
                    <input class="btn btn-primary btn-lg" value="Send Message" type="submit">
                </div>
            </div> --}}
        </div>

    </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1976.2099071081066!2d110.383926!3d-7.851055!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf299676f8a042a36!2sPondok+IT+Indonesia!5e0!3m2!1sen!2sid!4v1563519469239!5m2!1sen!2sid" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    {{-- <div id="map" class="to-animate"></div> --}}
</section>