<section id="fh5co-intro" style="padding-bottom:0;">
    <div class="container">
        <div class="row row-bottom-padded-lg">
            <div class="fh5co-block to-animate" style="background-image: url(https://images.unsplash.com/photo-1541432901042-2d8bd64b4a9b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=719&q=80);">
                <div class="overlay-darker"></div>
                <div class="overlay"></div>
                <div class="fh5co-text">
                    <i class="fh5co-intro-icon icon-book"></i>
                    <h2>Berilmu</h2>
                    <p>Mu’adz bin Jabal radhiyallaahu ‘anhu berkata:

                        الْعِلْمُ إمَامُ الْعَمَلِ وَالْعَمَلُ تَابِعُهُ
                        
                        “Ilmu adalah pemimpin amal, dan amal adalah pengikut ilmu” (Dari kitab al-Amru bil Ma’ruf wan nahyu anil munkar karya Ibnu Taimiyyah halaman 15).</p>
                    {{-- <p><a href="#" class="btn btn-primary">Get In Touch</a></p> --}}
                </div>
            </div>
            <div class="fh5co-block to-animate" style="background-image: url(https://images.unsplash.com/photo-1519834089823-08a494ba5a12?ixlib=rb-1.2.1&auto=format&fit=crop&w=668&q=80);">
                <div class="overlay-darker"></div>
                <div class="overlay"></div>
                <div class="fh5co-text">
                    <i class="fh5co-intro-icon icon-dashboard"></i>
                    <h2>Beramal</h2>
                    <p>“Barangsiapa yang mengerjakan amal-amal saleh, baik laki-laki maupun wanita sedang ia orang yang beriman, maka mereka itu masuk ke dalam surga dan mereka tidak dianiaya walau sedikitpun.” (QS. An-Nisa : 124)</p>
                    {{-- <p><a href="#" class="btn btn-primary">Click Me</a></p> --}}
                </div>
            </div>
            <div class="fh5co-block to-animate" style="background-image: url(https://images.unsplash.com/photo-1531804308561-b6438d25a810?ixlib=rb-1.2.1&auto=format&fit=crop&w=352&q=80);">
                <div class="overlay-darker"></div>
                <div class="overlay"></div>
                <div class="fh5co-text">
                    <i class="fh5co-intro-icon icon-heart"></i>
                    <h2>Bersabar</h2>
                    <p>“Dan, orang-orang yang sabar dalam kesempitan, penderitaan dan dalam peperangan, mereka itulah orang-orang yang benar (imannya), dan mereka itulah orang-orang yang bertaqwa”. (Al-Baqarah : 177).</p>
                    {{-- <p><a href="#" class="btn btn-primary">Why Us?</a></p> --}}
                </div>
            </div>
        </div>
        {{-- <div class="row watch-video text-center to-animate">
            <span>Watch the video</span>

            <a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo btn-video"><i class="icon-play2"></i></a>
        </div> --}}
    </div>
</section>