<section id="fh5co-home" data-section="home" style="background-image: url(https://images.unsplash.com/photo-1541432901042-2d8bd64b4a9b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=719&q=80);" data-stellar-background-ratio="0.5">
    <div class="gradient"></div>
    <div class="container">
        <div class="text-wrap">
            <div class="text-inner">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="to-animate">Manajemen Masjid</h1>
                        <h2 class="to-animate">Ayo bangun <a href="https://pondokit.com" target="_blank" title="Free HTML5 Bootstrap Templates">Masjid di Indonesia</a> Menjadi Lebih baik.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="slant"></div>
</section>