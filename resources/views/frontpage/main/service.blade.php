<section id="fh5co-services" data-section="services">
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-12 section-heading text-left">
                <h2 class=" left-border to-animate">Services</h2>
                <div class="row">
                    <div class="col-md-8 subtext to-animate">
                        <h3>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</h3>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-md-6 col-sm-6 fh5co-service to-animate">
                <i class="icon to-animate-2 icon-map-marker"></i>
                <h3>Kapanpun & dimanapun</h3>
                <p>Sistem berjalan secara web based kita bisa menjalankan kapanpun dan dimanapun tanpa batasan ruang dan waktu.</p>
            </div>
            <div class="col-md-6 col-sm-6 fh5co-service to-animate">
                <i class="icon to-animate-2 icon-calendar"></i>
                <h3>Jadwal Kegiatan</h3>
                <p>Mengatur dan mengelola kegiatan Masjid dengan rapi dan terjdawal. <a class="text-danger" href="#">Manajemen Masjid</a> memberikan kemudahan jamaah masjid.</p>
            </div>

            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-6 col-sm-6 fh5co-service to-animate">
                <i class="icon to-animate-2 icon-bar-chart"></i>
                <h3>Simple Accounting System</h3>
                <p>Membukukan keuangan masjid dengan fungsi pelaporan yang simple. <a class="text-danger" href="#">Manajemen Masjid</a> Memperkuat ekonomi dan transparansi</p>
            </div>
            <div class="col-md-6 col-sm-6 fh5co-service to-animate">
                <i class="icon to-animate-2 icon-cloud"></i>
                <h3>Cloud System</h3>
                <p>Tidak diperlukan kebutuhan system dan hardware secara khusus. Manajemen Masjid didukung cloud computing. Hanya dengan Komputer,laptop, android , Ios dan Internet</p>
            </div>
            <div class="col-md-6 col-sm-6 fh5co-service to-animate">
                <i class="icon to-animate-2 icon-newspaper-o"></i>
                <h3>Info dan Berita</h3>
                <p>Mendapatkan info dan berita serta artikel - artikel islami</p>
            </div>
            
        </div>
    </div>
</section>