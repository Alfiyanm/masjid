@extends('frontpage.main.master')
@include('frontpage.main.blog.header')
<div class="container-fluid p-5 pt-max bg-grad" height="50%">
        <div class="row">
            <div class="col-md-12">
                <div class="row text-main justify-content-center">
                    <h1 class="text-white" >{{$blog->title}}</h1>
                    <hr>
                    <div>
                        <img width="50%" src="{{ asset('storage/blog/'.$blog->photo)}}" alt="">
                    </div>
                    <div class="text-white">{!! $blog->content !!}</div>
                </div>
            </div>
        </div>
    </div>
@include('frontpage.main.footer')