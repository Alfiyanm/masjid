<div class="container-fluid p-5 pt-max bg-grad" height="50%">
    <h1 class="text-white">Blog</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="row text-main justify-content-center">
                @foreach ($blog as $news)
                <div class="col-md-3">
                    <div class="card">
                        <img src="{{ asset('storage/blog/'.$news->photo) }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h3 class="card-title text-main"> <a href="{{route('news.show',$news->slug)}}">{{ $news->title }}</a></h3>
                            <p class="card-text">{!! $news->content !!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>