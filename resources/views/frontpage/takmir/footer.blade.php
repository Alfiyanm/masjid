<footer id="footer" role="contentinfo">
    <a href="#" class="gotop js-gotop"><i class="icon-arrow-up2"></i></a>
    <div class="container">
        <div class="">
            <div class="col-md-12 text-center">
                <p>&copy; Elate Free HTML5. All Rights Reserved. <br>Created by <a href="http://freehtml5.co/" target="_blank">FREEHTML5.co</a> Editor by: <a href="https://www.facebook.com/ridwan.hasanah3" target="_blank">Ridwan Hasanah</a></p>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="social social-circle">
                    {{-- <li><a href="#"><i class="icon-twitter"></i></a></li> --}}
                    <li><a href="https://www.facebook.com/pondokprogrammer/"><i class="icon-facebook"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCIU-6AzrIiGk-hKRH_Wg1cA"><i class="icon-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>