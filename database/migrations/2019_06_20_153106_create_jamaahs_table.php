<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJamaahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jamaahs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('takmir_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('photo')->nullable();
            $table->text('address')->nullable();
            $table->string('province')->nullable();
            $table->string('hp')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('birth_place')->nullable();
            $table->enum('gender',['Pria','Wanita']);
            $table->timestamps();

            $table->foreign('takmir_id')
           ->references('id')
           ->on('takmirs')
           ->onUpdate('cascade')
           ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jamaahs');
    }
}
