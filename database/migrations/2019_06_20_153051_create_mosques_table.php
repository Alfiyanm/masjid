<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMosquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mosques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('takmir_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('photo')->nullable();
            $table->text('address')->nullable();
            $table->string('province')->nullable();
            $table->text('desc')->nullable();
            $table->timestamps();

            $table->foreign('takmir_id')
            ->references('id')
            ->on('takmirs')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosques');
    }
}
