<?php
require 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;

class JobdescOrgsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobdesc_orgs')->insert([
            'uuid' => Uuid::uuid4(),
            'takmir_id' => 1,
        ]);
    }
}
