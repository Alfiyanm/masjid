<?php

use Illuminate\Database\Seeder;

class MosquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mosques')->insert(array(
            [
                'name' => 'Al Amanah',
                'takmir_id' => 1,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Al Barokah',
                'takmir_id' => 2,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Al Husna',
                'takmir_id' => 3,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Baitur Rahman',
                'takmir_id' => 4,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Al Huda',
                'takmir_id' => 5,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Al Ikhlas',
                'takmir_id' => 6,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Jogokariyan',
                'takmir_id' => 7,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Gede Kauman',
                'takmir_id' => 8,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Pondok IT',
                'takmir_id' => 9,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ],
            [
                'name' => 'Al Amanah',
                'takmir_id' => 10,
                'province' => 'Jakarta',
                'address' => 'Pademangan Timur - Jakut',
            ]
        ));
    }
}
