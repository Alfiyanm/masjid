<?php
require 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
                'id' => 1,
                'uuid' => Uuid::uuid4(),
                'name' => 'Ridwan',
                'email_verified_at' => now(),
                'email' => 'master@pondokit.com',
                'password' => bcrypt('12345678'),
            ],
            [
                'id' => 2,
                'uuid' => Uuid::uuid4(),
                'name' => 'Admin',
                'email' => 'admin@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],
            [
                'id' => 3,
                'uuid' => Uuid::uuid4(),
                'name' => 'Ridwan',
                'email' => 'takmir@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 4,
                'uuid' => Uuid::uuid4(),
                'name' => 'Arief',
                'email' => 'takmir2@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 5,
                'uuid' => Uuid::uuid4(),
                'name' => 'Junaidi',
                'email' => 'takmir3@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 6,
                'uuid' => Uuid::uuid4(),
                'name' => 'Musa',
                'email' => 'takmir4@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 7,
                'uuid' => Uuid::uuid4(),
                'name' => 'Adi',
                'email' => 'takmir5@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 8,
                'uuid' => Uuid::uuid4(),
                'name' => 'Dayat',
                'email' => 'takmir6@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 9,
                'uuid' => Uuid::uuid4(),
                'name' => 'Daud',
                'email' => 'takmir7@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 10,
                'uuid' => Uuid::uuid4(),
                'name' => 'Lukman',
                'email' => 'takmir8@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 11,
                'uuid' => Uuid::uuid4(),
                'name' => 'Umar',
                'email' => 'takmir9@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ],[
                'id' => 12,
                'uuid' => Uuid::uuid4(),
                'name' => 'Ali',
                'email' => 'takmir10@pondokit.com',
                'email_verified_at' => now(),
                'password' => bcrypt('12345678'),
            ]
        ));
    }
}
