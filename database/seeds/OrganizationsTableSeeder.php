<?php
require 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->insert([
            'uuid' => Uuid::uuid4(),
            'takmir_id' => 1,
        ]);
    }
}
