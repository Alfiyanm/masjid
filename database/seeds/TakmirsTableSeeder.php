<?php

use Illuminate\Database\Seeder;

class TakmirsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('takmirs')->insert(array(
            [
                'user_id' => 3,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '085714442664'
            ],
            [
                'user_id' => 4,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '085734343464'
            ],
            [
                'user_id' => 5,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '08556655652664'
            ],
            [
                'user_id' => 6,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '08579769642664'
            ],
            [
                'user_id' => 7,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '0857144426789'
            ],
            [
                'user_id' => 8,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '0857144420000'
            ],
            [
                'user_id' => 9,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '085714442554564'
            ],
            [
                'user_id' => 10,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '0857687768678'
            ],
            [
                'user_id' => 11,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '08571446787686867'
            ],
            [
                'user_id' => 12,
                'province' => 'Jakarta',
                'gender' => 'Pria',
                'hp' => '08571678678678'
            ]
        ));
    }
}
